### Spring Cloud 脚手架

脚手架包含最基本核心以下组件：Euraka(注册中心)、Fegin(服务消费)、Ribbon(负载均衡)、Hystrix(熔断降级)、Gateway(网关)、Config&Bus(配置与消息总线)、Sleuth&Zipkin(服务追踪)；
 _(ps:有需要集成其他组件的欢迎评论区留言)_ 

**Linux环境下安装Docker版RabbitMQ**<br>
`docker run -d --hostname my-rabbit --name rabbit -p 15672:15672 -p 5672:5672 rabbitmq:management`

**Config Client动态刷新时需要执行请求（POST）来达到动态刷新配置值的效果**<br>
http://127.0.0.1:8888/actuator/bus-refresh

**Spring Cloud Sleuth 之Greenwich版本教程**<br>
https://my.oschina.net/xwzj/blog/4423547

 **关于Spring Cloud入门简介传送门** 
https://my.oschina.net/xwzj/blog/4433386